<?php
/**
 * Created by PhpStorm.
 * User: annagustafsson
 * Date: 2017-11-16
 * Time: 16:36
 */
$host = 'localhost';
$db = 'wieg16';
$user = 'root';
$password = 'root';
$charset = 'utf8';
$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$options = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false];

$pdo = new PDO($dsn, $user, $password, $options);
